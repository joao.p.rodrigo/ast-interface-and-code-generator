package pom

import (
	"fmt"
)

type Ava struct {
}

func (a Ava) Bark() error {
	_, err := fmt.Println("yap")
	return err
}

func (a Ava) Name() string {
	return "ava"
}
