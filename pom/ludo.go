package pom

import "fmt"

type Ludo struct {
	SignatureBark string
}

func (l Ludo) Bark() error {
	_, err := fmt.Println(l.SignatureBark)
	return err
}

func (l Ludo) Name() string {
	return "Ludo"
}
