package main

import (
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"os"
	"path"
	"strings"
)

type packageMatcher struct {
	interfaceDir    string
	targetInterface string
	implementerDir  string
	debug           bool
	methods         []string
}

func main() {
	implementerDir := flag.String("input", ".", "target path where the interface satisfiers are")
	iface := flag.String("interface", "", "interface that must be matched")
	methods := flag.String("methods", "", "methods that must be matched (if no interface name provided)")
	sourceTemplatePath := flag.String("template", "", "path for the source template")
	targetPath := flag.String("output", "", "path to emit the executed template")
	flag.Parse()

	fmt.Println("generating from", *implementerDir)

	matcher := packageMatcher{
		interfaceDir:    ".",
		targetInterface: *iface,
		implementerDir:  *implementerDir,
	}

	if *iface != "" {
		methods, err := matcher.parseInterfaceMethods()
		if err != nil {
			fatal(fmt.Errorf("parse interface methods: %v", err))
		}

		matcher.methods = methods
	} else {
		matcher.methods = methodIdentifiers(*methods)
	}

	if len(matcher.methods) == 0 {
		fatal(errors.New("must have at least one method to match"))
	}

	matchedTypes, err := matcher.parseImplementers()
	if err != nil {
		fatal(err)
	}

	if err := RunTemplateFromPath(*sourceTemplatePath, *targetPath, matchedTypes); err != nil {
		fatal(err)
	}
}

func methodIdentifiers(methods string) []string {
	methodList := strings.Split(methods, ",")
	cleanMethods := methodList[:0]
	for _, m := range methodList {
		cleanMethod := strings.TrimSpace(m)
		if cleanMethod == "" {
			continue
		}

		cleanMethods = append(cleanMethods, cleanMethod)
	}

	if len(cleanMethods) == 0 {
		fatal(errors.New("you must pass at least one method"))
	}

	return cleanMethods
}

func fatal(err error) {
	fmt.Println(err)
	os.Exit(1)
}

func targetFiles(fileSystem fs.FS, basePath string) []string {
	matches, _ := fs.Glob(fileSystem, "*.go")

	for i, m := range matches {
		matches[i] = path.Join(basePath, m)
	}

	return matches
}
