package main

import (
	"bytes"
	"errors"
	"io"
	"os"
	"text/template"
)

func RunTemplateFromPath(sourcePath string, targetPath string, data any) error {
	if sourcePath == "" {
		return errors.New("source must not be empty")
	}

	t, err := template.New(sourcePath).ParseFiles(sourcePath)
	if err != nil {
		return err
	}

	var w io.Writer
	if targetPath != "" {
		f, err := os.Create(targetPath)
		if err != nil {
			return err
		}

		defer f.Close()
		w = f
	} else {
		w = os.Stdout
	}

	err = runTemplate(t, w, data)

	return err
}

func runTemplate(t *template.Template, w io.Writer, data any) error {
	buf := bytes.Buffer{}
	err := t.Execute(&buf, data)
	if err != nil {
		return err
	}

	_, err = buf.WriteTo(w)
	return err
}
