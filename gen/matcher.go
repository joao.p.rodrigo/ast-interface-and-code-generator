package main

import (
	"errors"
	"go/ast"
	"go/parser"
	"go/token"
	"slices"
)

type methodFunc struct {
	ReceiverType string
	MethodName   string
}

type interfaceDecl struct {
	name    string
	methods []string
}

func (m *packageMatcher) parseImplementers() ([]string, error) {
	fset := token.NewFileSet()
	pkgs, err := parser.ParseDir(fset, m.implementerDir, nil, parser.SkipObjectResolution)
	if err != nil {
		return nil, err
	}

	all := matchedMethods{}
	for _, pkg := range pkgs {
		for _, file := range pkg.Files {
			m.parseImplementerFile(file, all)
		}
	}
	return all.AllThatSatisfy(m.methods), nil
}

func (m *packageMatcher) parseInterfaceMethods() ([]string, error) {
	fset := token.NewFileSet()

	ms, err := parser.ParseDir(fset, m.interfaceDir, nil, parser.SkipObjectResolution)
	if err != nil {
		return nil, err
	}

	for _, pkg := range ms {
		for _, file := range pkg.Files {
			for _, decl := range file.Decls {
				iface, ok := matchInterface(decl)
				if !ok || iface.name != m.targetInterface {
					continue
				}

				return iface.methods, nil
			}
		}
	}

	return nil, errors.New("interface not found")
}

func (m *packageMatcher) parseImplementerFile(file *ast.File, matchedMethods matchedMethods) {
	for _, decl := range file.Decls {
		mt, ok := matchMethod(decl)
		if !ok {
			continue
		}

		if slices.Contains(m.methods, mt.MethodName) {
			matchedMethods.Add(mt.MethodName, mt.ReceiverType)
		}
	}
}

func matchInterface(decl ast.Decl) (interfaceDecl, bool) {
	var iDecl interfaceDecl

	genDecl, ok := decl.(*ast.GenDecl)
	if !ok {
		return iDecl, false
	}

	if genDecl.Tok != token.TYPE {
		return iDecl, false
	}

	for _, spec := range genDecl.Specs {
		typeSpec, ok := spec.(*ast.TypeSpec)
		if !ok {
			return iDecl, false
		}

		iDecl.name = typeSpec.Name.Name

		iFaceType, ok := typeSpec.Type.(*ast.InterfaceType)
		if !ok {
			return iDecl, false
		}

		for _, method := range iFaceType.Methods.List {
			var methodName string
			for _, name := range method.Names {
				methodName = name.Name
			}

			iDecl.methods = append(iDecl.methods, methodName)
		}
	}

	return iDecl, true
}

func matchMethod(decl ast.Decl) (methodFunc, bool) {
	var method methodFunc

	fnDecl, ok := decl.(*ast.FuncDecl)
	if !ok {
		return method, false
	}

	if fnDecl.Recv == nil {
		return method, false
	}

	method.ReceiverType = receiverTypeName(fnDecl.Recv)
	if method.ReceiverType == "" {
		return method, false
	}

	method.MethodName = fnDecl.Name.Name

	return method, true
}

func receiverTypeName(r *ast.FieldList) string {
	for _, field := range r.List {
		ident := field.Type.(*ast.Ident)
		return ident.Name
	}

	return ""
}
