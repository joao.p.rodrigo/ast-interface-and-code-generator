package main

type matchKey struct {
	methodName  string
	matchedType string
}

type matchedMethods map[matchKey]bool

func (m matchedMethods) Add(methodName, matchedType string) {
	m[matchKey{methodName, matchedType}] = true
}

func (m matchedMethods) AllThatSatisfy(methods []string) []string {
	allTypes := map[string]struct{}{}
	for matches := range m {
		allTypes[matches.matchedType] = struct{}{}
	}

	var typesThatSatisfy []string

	for t := range allTypes {
		satisfiesAll := true
		for _, method := range methods {
			if !m[matchKey{method, t}] {
				satisfiesAll = false
				break
			}
		}

		if satisfiesAll {
			typesThatSatisfy = append(typesThatSatisfy, t)
		}
	}

	return typesThatSatisfy
}
