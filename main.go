package main

import (
	"bytes"
	"encoding/json"
	"fmt"
)

//go:generate go run ./gen -input=./pom -interface=Barker -template=switch.tmpl -output=loader.go

type Barker interface {
	Name() string
	Bark() error
}

func main() {
	ludo, err := switchLoader("Ludo", []byte(`{"SignatureBark":"yyip"}`))
	if err != nil {
		panic(err)
	}
	fmt.Println(ludo.Bark())

	ava, err := switchWithMap("ava", []byte(`{}`))
	if err != nil {
		panic(err)
	}
	fmt.Println(ava.Bark())
}

func loadJSON(barker Barker, config []byte) (Barker, error) {
	dec := json.NewDecoder(bytes.NewReader(config))
	dec.DisallowUnknownFields()
	return barker, dec.Decode(barker)
}
