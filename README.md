# Code generation from interface and implementers

This project exemplifies how to generate code based on an interface and a package
with types that implement it.

This is not intended as production code, but should work as expected.

The companion blog post can be read in my personal blog 
[notadoctor.me](http://notadoctor.me/posts/playing_with_the_ast/).

There is no attached License but I humbly request that if you use my code, star it
or let me know I helped someone. Ideally make a donation to your local animal shelter.

## Background

There is an interface `Barker` and we want to generate a deserializer
that takes a name and json config and gives back an appropriate struct
that implements the interface.

Ludo and Ava are pomeranians from a very old youtube clip. I tried to
find it but wasn't able to. But they will be our Barkers for this.

## How to use

Run the following commands (after inspecting the code):

```sh
go generate ./...
go run .
```

